#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:32 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "Arabski (Egipt)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "Arabski (Tunizija)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr ""

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "Bolgarski (Bolgarija)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "Katalonski (Katalonija, Španija)"

#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr "Valižanski (Združeno kraljestvo)"

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr ""

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "Danski (Danska)"

#: Language.class:40
msgid "German (Germany)"
msgstr "Nemški (Nemčija)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "Nemški (Belgija)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "Grški (Grčija)"

#: Language.class:47
msgid "English (common)"
msgstr "Angleški (splošno)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "Angleški (Velika Britanija)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "Angleški (ZDA)"

#: Language.class:50
msgid "English (Australia)"
msgstr "Angleški (Avstralija)"

#: Language.class:51
msgid "English (Canada)"
msgstr "Angleški (Kanada)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr ""

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "Španski (Španija)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "Španski (Argentina)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr ""

#: Language.class:65
msgid "Basque (Basque country)"
msgstr ""

#: Language.class:68
msgid "Farsi (Iran)"
msgstr ""

#: Language.class:71
msgid "Finnish (Finland)"
msgstr ""

#: Language.class:74
msgid "French (France)"
msgstr "Francoski (Francija)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "Francoski (Belgija)"

#: Language.class:76
msgid "French (Canada)"
msgstr "Francoski (Kanada)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "Francoski (Švica)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "Galicijski (Španija)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr ""

#: Language.class:86
msgid "Hindi (India)"
msgstr ""

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "Madžarski (Madžarska)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "Hrvaški (Hrvaška)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "Indonezijski (Indonezija)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "Irski (Irska)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

#: Language.class:104
msgid "Italian (Italy)"
msgstr "Italijanski (Italija)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr ""

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr ""

#: Language.class:113
msgid "Korean (Korea)"
msgstr ""

#: Language.class:116
msgid "Latin"
msgstr "Latin"

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr ""

#: Language.class:122
msgid "Malayalam (India)"
msgstr ""

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr ""

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "Nizozemski (Nizozemska)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "Nizozemski (Belgija)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "Norveški (Norveška)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr ""

#: Language.class:138
msgid "Polish (Poland)"
msgstr "Poljski (Poljska)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "Portugalski (Portugalska)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "Portugalski (Brazilija)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr ""

#: Language.class:148
msgid "Romanian (Romania)"
msgstr ""

#: Language.class:151
msgid "Russian (Russia)"
msgstr "Ruski (Rusija)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "Slovenski (Slovenija)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr ""

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr ""

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "Švedski (Švedska)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "Turški (Turčija)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr ""

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr ""

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "Valonski (Belgija)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "Poenostavljena kitajščina (Kitajska)"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "Tradicionalna kitajščina (Tajvan)"

#: Language.class:241
msgid "Unknown"
msgstr "Neznano"
